//Exercício 1
//Faça um código que gere uma array de tamanho qualquer com números aleatórios de 0 a 100, e em seguida, ordene esse array utilizando um algoritmo de sua preferência. 
//Obs: Implementar algoritmo de ordenação manualmente, sem usar o método sort().

let num = [];
let tam = Math.floor((Math.random()+1)*22);
//gera um array de tamanho aleatório entre 1 e 50
console.log("Array desordenado");
for(let i=0; i < tam; i++){
    num[i]=Math.floor(Math.random()*51);
    console.log(num[i]);
}

//Preencher o array com valores aleatórios
for(let i = 0; i < num.length; i++){
    for(let j = 0; j < num.length; j++){
        if(num[i] < num[j]){
            let temp = num[i];
            num[i]=num[j];
            num[j]=temp;
        }
    }
}

console.log("\n\n---------\n\nArray ordenado");
for(let i=0; i < num.length; i++){
    console.log(num[i]);
}

